<?php

namespace Drupal\breakpoints_extra;

use \Drupal\breakpoint\Breakpoint;
use \Drupal\breakpoint\BreakpointInterface;

/**
 * Default object used for breakpoint plugins.
 *
 * @see \Drupal\breakpoints_extra\Breakpoint
 * @see plugin_api
 */
class BreakpointsExtra extends Breakpoint implements BreakpointInterface {

  /**
   * @return array
   */
  public function getExtras() {
    if(!empty($this->pluginDefinition['extras'])){
      return $this->pluginDefinition['extras'];
    } else {
      return [];
    }

  }

  /**
   * @return array
   */
  public function getExtra($key) {
    if(isset($this->pluginDefinition['extras'][$key])){
      return $this->pluginDefinition['extras'][$key];
    } else {
      return null;
    }
  }
}
