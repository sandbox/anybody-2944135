<?php

namespace Drupal\breakpoints_extra;

use \Drupal\breakpoint\BreakpointManager;
use \Drupal\breakpoint\BreakpointManagerInterface;
use \Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Defines a breakpoint plugin manager to deal with breakpoints.
 *
 * Extension can define breakpoints in a EXTENSION_NAME.breakpoints.yml file
 * contained in the extension's base directory. Each breakpoint has the
 * following structure:
 * @code
 *   MACHINE_NAME:
 *     label: STRING
 *     mediaQuery: STRING
 *     weight: INTEGER
 *     multipliers:
 *       - STRING
 *     extras:
 *       minWidth: 600
 * @endcode
 * For example:
 * @code
 * bartik.mobile:
 *   label: mobile
 *   mediaQuery: '(min-width: 0px)'
 *   weight: 0
 *   multipliers:
 *     - 1x
 *     - 2x
 *   extras:
 *     minWidth: 640
 *     maxWidth: 1400
 * @endcode
 * Optionally a breakpoint can provide a group key. By default an extensions
 * breakpoints will be placed in a group labelled with the extension name.
 *
 * @see \Drupal\breakpoint\Breakpoint
 * @see \Drupal\breakpoint\BreakpointInterface
 * @see plugin_api
 */
class BreakpointsExtraManager extends BreakpointManager implements BreakpointManagerInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaults = [
    // Human readable label for breakpoint.
    'label' => '',
    // The media query for the breakpoint.
    'mediaQuery' => '',
    // Weight used for ordering breakpoints.
    'weight' => 0,
    // Breakpoint multipliers.
    'multipliers' => [],
    // The breakpoint group.
    'group' => '',
    // Default class for breakpoint implementations.
    'class' => 'Drupal\breakpoint\Breakpoint',
    // The plugin id. Set by the plugin system based on the top-level YAML key.
    'id' => '',
    // Extras
    'extras' => [],
  ];

}
