<?php

/**
 * @file
 * Contains \Drupal\breakpoints_extra\Form\DrowlParagraphsSettingsForm.
 */

namespace Drupal\breakpoints_extra\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Administration settings form.
 */
class BreakpointsExtraAdminForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'breakpoints_extra_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('breakpoints_extra.settings');
    $settings = $config->get();

    $form['expose_to_js'] = array(
      '#type' => 'checkbox',
      '#title' => $this
        ->t('Expose breakpoints to JavaScript'),
      '#description' => $this
        ->t('Exposes the breakpoint groups to JavaScript settings as Drupal.breakpoints array.'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('breakpoints_extra.settings');
    $form_values = $form_state->getValues();
    $config->set('expose_to_js', $form_values['expose_to_js'])
      ->save();
    parent::submitForm($form, $form_state);
  }
}
